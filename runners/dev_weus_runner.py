from copy import copy
import random as rand
from warnings import warn
import traceback
import time
import logging

import numpy as np

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

from wepy.walker import Walker, WalkerState
from wepy.runners.runner import Runner
from wepy.runners.openmm import OpenMMRunner
from wepy.runners.openmm import OpenMMState
from wepy.work_mapper.worker import Worker
from wepy.reporter.reporter import Reporter
from eliot import log_call, start_action

## Constants

# NEW_KEYS = ('positions', 'velocities', 'forces', 'kinetic_energy',
#         'potential_energy', 'time', 'box_vectors', 'box_volume',
#             'parameters', 'parameter_derivatives', 'activity')

# when we use the get_state function from the simulation context we
# can pass options for what kind of data to get, this is the default
# to get all the data. TODO not really sure what the 'groups' keyword
# is for though
GET_STATE_KWARG_DEFAULTS = (('getPositions', True),
                            ('getVelocities', True),
                            ('getForces', True),
                            ('getEnergy', True),
                            ('getParameters', True),
                            ('getParameterDerivatives', True),
                            ('enforcePeriodicBox', True),)

# the Units objects that OpenMM uses internally and are returned from
# simulation data
UNITS = (('positions_unit', unit.nanometer),
         ('time_unit', unit.picosecond),
         ('box_vectors_unit', unit.nanometer),
         ('velocities_unit', unit.nanometer/unit.picosecond),
         ('forces_unit', unit.kilojoule / (unit.nanometer * unit.mole)),
         ('box_volume_unit', unit.nanometer),
         ('kinetic_energy_unit', unit.kilojoule / unit.mole),
         ('potential_energy_unit', unit.kilojoule / unit.mole),
        )

# the names of the units from the units objects above. This is used
# for saving them to files
UNIT_NAMES = (('positions_unit', unit.nanometer.get_name()),
         ('time_unit', unit.picosecond.get_name()),
         ('box_vectors_unit', unit.nanometer.get_name()),
         ('velocities_unit', (unit.nanometer/unit.picosecond).get_name()),
         ('forces_unit', (unit.kilojoule / (unit.nanometer * unit.mole)).get_name()),
         ('box_volume_unit', unit.nanometer.get_name()),
         ('kinetic_energy_unit', (unit.kilojoule / unit.mole).get_name()),
         ('potential_energy_unit', (unit.kilojoule / unit.mole).get_name()),
        )

# a random seed will be chosen from 1 to RAND_SEED_RANGE_MAX when the
# Langevin integrator is created. 0 is the default and special value
# which will then choose a random value when the integrator is created
RAND_SEED_RANGE_MAX = 1000000

class OpenMMWindowExchangeRunner(OpenMMRunner):
    #
    # Same as OpenMMRunner, but calculates an "activity" increment (work)
    # using the final frames of each relevant segment, and adds
    # this to the action associated with each state. This differs from the
    # Work runner above by calculating the work based on the potential
    # (U-current and U-future) only at cycles where the lambda parameter,
    # aka the d0 value is modified.
    #
    
    def __init__(self, system, topology, integrator, platform=None, enforce_box=True, harmonic_force=None):
        
        super().__init__(system, topology, integrator, platform=platform, enforce_box=enforce_box)

        # metric function object
        self.KEYS = ('positions', 'velocities', 'forces', 'kinetic_energy',
                     'potential_energy', 'time', 'box_vectors', 'box_volume',
                     'parameters', 'parameter_derivatives', 'd0_val')

        # define the activity metric for the system
#        self.activity_metric = activity_metric
        self.platform = platform
        # define the time dependent force that will applied to the system
        self.harmonic_force = harmonic_force
        
        # variables that get checked or changed each cycle
        self._cycle_idx = None
        self._params = {}
                
    def _update_forces(self, system, d0_val):

        # update the forces for running with the updated time dep. forces in the next cycle
        updated_system, params = self.harmonic_force.change_force_params(system, d0_val)

        return updated_system
                
    def generate_state(self, simulation, segment_lengths, starting_walker, getState_kwargs):

        new_sim_state = simulation.context.getState(**getState_kwargs)

        # get the d0 value assocaited with this walker
        d0_val = starting_walker.state['d0_val']

        new_state = OpenMMState(new_sim_state, d0_val=np.array(d0_val))

        return new_state

 
    def run_segment(self,
                    walker,
                    segment_length,
                    getState_kwargs=None,
                    platform=None,
                    platform_kwargs=None,
                    **kwargs):
        """Run dynamics for the walker.

        Parameters
        ----------
        walker : object implementing the Walker interface
            The walker for which dynamics will be propagated.

        segment_length : int or float
            The numerical value that specifies how much dynamics are to be run.

        getState_kwargs : dict of str : bool, optional
            Specify the key-word arguments to pass to
            simulation.context.getState when getting simulation
            states. If None defaults object values.

        platform : str or None or Ellipsis
            The specification for the computational platform to
            use. If None will use the default for the runner and
            ignore platform_kwargs. If Ellipsis forces the use of the
            OpenMM default or environmentally defined platform. See
            OpenMM documentation for all value but typical ones are:
            Reference, CUDA, OpenCL. If value is None the automatic
            platform determining mechanism in OpenMM will be used.

        platform_kwargs : dict of str : bool, optional
            key-values to set for a platform with
            platform.setPropertyDefaultValue for this segment only.


        Returns
        -------
        new_walker : object implementing the Walker interface
            Walker after dynamics was run, only the state should be modified.

        """
        run_segment_start = time.time()

        # set the kwargs that will be passed to getState
        tmp_getState_kwargs = getState_kwargs

        logging.info("Default 'getState_kwargs' in runner: "
                     f"{self.getState_kwargs}")

        logging.info("'getState_kwargs' passed to 'run_segment' : "
                     f"{getState_kwargs}")

        # start with the object value
        getState_kwargs = copy(self.getState_kwargs)
        if tmp_getState_kwargs is not None:
            getState_kwargs.update(tmp_getState_kwargs)

        logging.info("After resolving 'getState_kwargs' that will be used are: "
                     f"{getState_kwargs}")


        gen_sim_start = time.time()

        # make a copy of the integrator for this particular segment
        new_integrator = copy(self.integrator)
        # force setting of random seed to 0, which is a special
        # value that forces the integrator to choose another
        # random number
        new_integrator.setRandomNumberSeed(0)

        ## Platform

        logging.info("Default 'platform' in runner: "
                     f"{self.platform_name}")

        logging.info("pre_cycle set 'platform' in runner: "
                     f"{self._cycle_platform}")

        logging.info("'platform' passed to 'run_segment' : "
                     f"{platform}")

        logging.info("Default 'platform_kwargs' in runner: "
                     f"{self.platform_kwargs}")

        logging.info("pre_cycle set 'platform_kwargs' in runner: "
                     f"{self._cycle_platform_kwargs}")


        logging.info("'platform_kwargs' passed to 'run_segment' : "
                     f"{platform_kwargs}")

        platform_name, platform_kwargs = self._resolve_platform(
            platform, platform_kwargs
        )


        logging.info("Resolved 'platform' : "
                     f"{platform_name}")

        logging.info("Resolved 'platform_kwargs' : "
                     f"{platform_kwargs}")



        # create simulation object

        ## create the platform and customize



        # if a platform was given we use it to make a Simulation object
        if platform_name is not None:

            logging.info("Using platform configured in code.")

            # get the platform by its name to use
            platform = omm.Platform.getPlatformByName(platform_name)

            logging.info(f"Platform object created: {platform}")

            if platform_kwargs is None:
                platform_kwargs = {}

            # set properties from the kwargs if they apply to the platform
            for key, value in platform_kwargs.items():

                if key in platform.getPropertyNames():

                    logging.info(f"Setting platform property: {key} : {value}")
                    platform.setPropertyDefaultValue(key, value)

                else:
                    warn(f"Platform kwargs given ({key} : {value}) "
                         f"but is not valid for this platform ({platform_name})")

            new_system = copy(self.system)
            d0_val = walker.state['d0_val']
            new_system  = self._update_forces(new_system, d0_val)

            # make a new simulation object
            simulation = omma.Simulation(self.topology, new_system,
                                         new_integrator, platform) 

        # otherwise just use the default or environmentally defined one
        else:
            logging.info("Using environmental platform.")
            simulation = omma.Simulation(self.topology, self.system,
                                         new_integrator)

        # set the state to the context from the walker
        simulation.context.setState(walker.state.sim_state)

        gen_sim_end = time.time()
        gen_sim_time = gen_sim_end - gen_sim_start

        logging.info("Time to generate the system: {}".format(gen_sim_time))

        # actually run the simulation

        steps_start = time.time()

        # Run the simulation segment for the number of time steps
        with start_action(action_type="OpenMM Simulation.steps") as ommsim_cx:
            simulation.step(segment_length)

        steps_end = time.time()
        steps_time = steps_end - steps_start


        logging.info("Time to run {} sim steps: {}".format(segment_length, steps_time))


        get_state_start = time.time()


        get_state_end = time.time()
        get_state_time = get_state_end - get_state_start
        logging.info("Getting context state time: {}".format(get_state_time))

        # generate the new state/walker
        new_state = self.generate_state(simulation, segment_length,
                                        walker, getState_kwargs)
        
        # create a new walker for this
        new_walker = OpenMMWalker(new_state, walker.weight)

        run_segment_end = time.time()
        run_segment_time = run_segment_end - run_segment_start
        logging.info("Total internal run_segment time: {}".format(run_segment_time))


        segment_split_times = {
            'gen_sim_time' :  gen_sim_time,
            'steps_time' : steps_time,
            'get_state_time' : get_state_time,
            'run_segment_time' : run_segment_time,
        }

        self._last_cycle_segments_split_times.append(segment_split_times)

        return new_walker

    # @log_call(include_args=['getState_kwargs'],
    #           include_result=False)



class OpenMMWalker(Walker):
    """Walker for OpenMMRunner simulations.                                                                    This simply enforces the use of an OpenMMState object for the                                              walker state attribute.                                                                                    """

    def __init__(self, state, weight):
        # documented in superclass                                                                         
        assert isinstance(state, OpenMMState), \
            "state must be an instance of class OpenMMState not {}".format(type(state))

        super().__init__(state, weight)

