import numpy as np
import mdtraj as mdj
import sys
from wepy.hdf5 import WepyHDF5

pdb_path = sys.argv[1]
hdf5_path = sys.argv[2]
name = sys.argv[3]

pdb = mdj.load_pdb(pdb_path)
wepy_h5 = WepyHDF5(hdf5_path, mode='r')
wepy_h5.open()

n_cycles = wepy_h5.num_run_cycles(0)
print(n_cycles)
n_walkers = wepy_h5.num_trajs
print(n_walkers)
n_atoms = wepy_h5.num_atoms

#walker_idx = 0
positions = np.zeros(shape=(n_cycles*n_walkers, n_atoms, 3))

unitcell_lengths = np.zeros(shape=(n_walkers*n_cycles,3))
for i in range(n_walkers*n_cycles):
    for j in range(3):
        unitcell_lengths[i][j] += 4.31

unitcell_angles = np.zeros(shape=(n_walkers*n_cycles,3))
for i in range(n_walkers*n_cycles):
    for j in range(3):
        unitcell_angles[i][j] += 90


for i in range(n_walkers):
    positions[i*n_cycles : i*n_cycles+n_cycles] += np.array(wepy_h5.h5[f'runs/0/trajectories/{i}/positions'])

traj = mdj.Trajectory(positions, pdb.topology,
                          unitcell_lengths=unitcell_lengths,
                          unitcell_angles=unitcell_angles)

#    traj.superpose(traj, frame=0, atom_indices=HOSTIDXS)
traj.image_molecules()

traj = traj.center_coordinates()
traj.save_dcd(f'{name}')
print('DCD done')



wepy_h5.close()
