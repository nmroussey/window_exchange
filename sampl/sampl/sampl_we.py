import sys
from copy import copy
import os
import os.path as osp
import pickle

import numpy as np

from simtk.openmm.app import *
from simtk.openmm import *
from simtk.unit import *

import simtk.openmm as omm
import simtk.unit as unit

#from openmmtools.testsystems import LennardJonesPair
import mdtraj as mdj
from wepy.util.mdtraj import mdtraj_to_json_topology

from wepy.sim_manager import Manager

from distances.one_prop_dist import OnePropertyDistance
from activities.openmm import SAMPLHarmonicPotential
from resamplers.activity_revo import ActivityResampler
from forces.dev_sampl_harmonic import HarmonicForce

from wepy.walker import Walker
from wepy.runners.openmm import OpenMMRunner, OpenMMState, OpenMMGPUWorker
from runners.dev_sampl_runner import OpenMMHarmonicSMAPLRunner
from wepy.runners.openmm import UNIT_NAMES, GET_STATE_KWARG_DEFAULTS
from wepy.work_mapper.mapper import Mapper
from wepy.work_mapper.mapper import WorkerMapper

from wepy.reporter.hdf5 import WepyHDF5Reporter

# Platform used for OpenMM which uses different hardware computation
# kernels. Options are: Reference, CPU, OpenCL, CUDA.
PLATFORM = 'Reference'

# Monte Carlo Barostat
PRESSURE = 1.0*unit.atmosphere
TEMPERATURE = 300.0*unit.kelvin
FRICTION_COEFFICIENT = 1/unit.picosecond
STEP_SIZE = 0.002*unit.picoseconds

# the maximum weight allowed for a walker
PMAX = 0.5
# the minimum weight allowed for a walker
PMIN = 1e-100

# reporting parameters

# these are the properties of the states (i.e. from OpenMM) which will
# be saved into the HDF5
SAVE_FIELDS = ('positions', 'box_vectors', 'velocities', 'activity')
# these are the names of the units which will be stored with each
# field in the HDF5
UNITS = UNIT_NAMES
ALL_ATOMS_SAVE_FREQ = 5
#SPARSE_FIELDS = (('velocities', 5),
#                )

## INPUTS/OUTPUTS

# the inputs directory
inputs_dir = osp.realpath('./sampl_system')
# the outputs path
outputs_dir = osp.realpath('./sampl_outputs')
# make the outputs dir if it doesn't exist
os.makedirs(outputs_dir, exist_ok=True)

#import ipdb; ipdb.set_trace()

# inputs filenames
json_top_filename = "host_guest_OA-G6-0.top.json"

# outputs
hdf5_filename = 'sampl_test.wepy.h5'

# normalize the input paths
json_top_path = osp.join(inputs_dir, json_top_filename)

# normalize the output paths
hdf5_path = osp.join(outputs_dir, hdf5_filename)

# initiate the test system
# test_sys = LennardJonesPair()
# test_sys.positions[1,0] = 4.0*unit.angstrom

# # change the box size
# # Vectors set in Vec3. unit=nanometers
# test_sys.system.setDefaultPeriodicBoxVectors([4,0,0],[0,4,0],[0,0,4])
# #import ipdb; ipdb.set_trace()
# system = test_sys.system
# omm_topology = test_sys.topology
# mdj_top = mdj.Topology.from_openmm(omm_topology)
# json_top = mdtraj_to_json_topology(mdj_top)

# make the integrator and barostat
integrator = omm.LangevinIntegrator(TEMPERATURE, FRICTION_COEFFICIENT, STEP_SIZE)

# build the harmonic force
total_lj = CustomBondForce("0.5*k*(r-r0)^2")
total_lj.addPerBondParameter('k')
total_lj.addPerBondParameter('r0')
# param [0] is the spring constant, param [1] is the init distance (nm)
total_lj.addBond(0, 1, [2000, 0.4*unit.nanometer])
system.addForce(total_lj)

# make a context and set the positions
context = omm.Context(test_sys.system, integrator)
context.setPositions(test_sys.positions)

omm_state = context.getState()
array_pos = test_sys.positions._value

# get the data from this context so we have a state to start the
# simulation with
get_state_kwargs = dict(GET_STATE_KWARG_DEFAULTS)
init_sim_state = context.getState(**get_state_kwargs)
init_state = OpenMMState(init_sim_state, activity=np.array([0.]))

# initialize the runner; get necessary force values
delta_dist = 0.032 #0.0075 #nm

per_bond_params = total_lj.getBondParameters(0)
k_init = per_bond_params[2][0]
r0_init = per_bond_params[2][1]

runner = OpenMMHarmonicPotentialRunner(system, omm_topology,
                                  integrator, platform=PLATFORM,
                                  activity_metric=LJHarmonicPotential(topology=mdj_top),
                                  harmonic_force=HarmonicForce(delta_dist=delta_dist,
                                                               k_init=k_init, r0_init=r0_init))

distance = OnePropertyDistance('activity')

## Resampler
resampler = ActivityResampler(distance=distance, init_state=init_state)#, propname='activity')

## Reporters
# make a dictionary of units for adding to the HDF5
units = dict(UNIT_NAMES)

hdf5_reporter = WepyHDF5Reporter(file_path=hdf5_path, mode='w',
                                 # save_fields set to None saves everything
                                 save_fields=None,
                                 resampler=resampler,
                                 boundary_conditions=None,
                                 topology=json_top,
                                 units=units)
reporters = [hdf5_reporter]

mapper = Mapper()

## Run the simulation

if __name__ == "__main__":

    if sys.argv[1] == "-h" or sys.argv[1] == "--help":
        print("arguments: n_cycles, n_steps, n_walkers")
    else:
        n_cycles = int(sys.argv[1])
        n_steps = int(sys.argv[2])
        n_walkers = int(sys.argv[3])

        print("Number of steps: {}".format(n_steps))
        print("Number of cycles: {}".format(n_cycles))
        # # create the initial walkers
        init_weight = 1.0 / n_walkers
        init_walkers = [Walker(init_state, init_weight) for i in range(n_walkers)]

        sim_manager = Manager(init_walkers,
                              runner=runner,
                              resampler=resampler,
                              boundary_conditions=None,
                              work_mapper=mapper,
                              reporters=reporters)

        # make a number of steps for each cycle. In principle it could be
        # different each cycle
        steps = [n_steps for i in range(n_cycles)]

       # actually run the simulation
        print("Starting run: {}".format(0))
        sim_manager.run_simulation(n_cycles, steps)
        print("Finished run: {}".format(0))


        print("Finished first file")
