import pickle

import sys
import h5py
import numpy as np
import os
import os.path as osp

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

import mdtraj as mdj
from mdtraj.reporters import DCDReporter
from copy import copy
from copy import deepcopy

if __name__ == "__main__":

    # SETUP ------------------------------------------------------

    # setup system using Gromacs files

    # set the string identifier for the platform to be used by openmm
    platform = 'CUDA'

    tag = sys.argv[1]

    pdbfile = osp.join('/mnt/home/alexrd/sampl6/host_guest/SAMPLing',tag,'complex.pdb')
    grofile = osp.join('/mnt/home/alexrd/sampl6/host_guest/SAMPLing',tag,'complex.gro')
    topfile = osp.join('/mnt/home/alexrd/sampl6/host_guest/SAMPLing',tag,'complex.top')
    
    pdb = mdj.load_pdb(pdbfile)
    gro = omma.GromacsGroFile(grofile)
    top = omma.GromacsTopFile(topfile, periodicBoxVectors=gro.getPeriodicBoxVectors())

    rests = [0.4,0.8,1.2]

    for rest_d in rests:
        system = top.createSystem(nonbondedMethod=omma.PME, nonbondedCutoff=1*unit.nanometer,
                                  constraints=omma.HBonds)

        # add external force to simulation
        hst_idxs = pdb.topology.select('resname "HST"')
        gst_idxs = pdb.topology.select('resname "GST"')
        centForce = omm.openmm.CustomCentroidBondForce(2, "0.5*k*(distance(g1,g2)-d0)^2")
        centForce.addPerBondParameter('k')
        centForce.addPerBondParameter('d0')
        centForce.addGroup([int(i) for i in hst_idxs])
        centForce.addGroup([int(i) for i in gst_idxs])
        centForce.addBond([0, 1], [2000, rest_d*unit.nanometer])

        system.addForce(centForce)

        # make an integrator object that is constant temperature
        integrator = omm.LangevinIntegrator(300*unit.kelvin,
                                            1/unit.picosecond,
                                            0.002*unit.picoseconds)

        simulation = omma.Simulation(top.topology, system, integrator, omm.Platform.getPlatformByName(platform))
        simulation.reporters.append(DCDReporter('{0}_{1}.dcd'.format(tag,rest_d),1000))
        simulation.context.setPositions(gro.positions)
        simulation.minimizeEnergy()
        simulation.step(10000)
