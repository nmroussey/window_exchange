import os
import os.path as osp
import pickle
import logging
import multiprocessing as mp
import numpy as np
import mdtraj as mdj

# OpenMM libraries for setting up simulation objects and loading
# the forcefields
import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit
from collections import namedtuple
## Wepy classes

# the simulation manager and work mapper for actually running the simulation
from wepy.sim_manager import Manager
from wepy.work_mapper.mapper import WorkerMapper

# the runner for running dynamics and making and it's particular
# state class
from wepy.runners.openmm import OpenMMRunner, OpenMMState, OpenMMGPUWorker, UNIT_NAMES
from wepy.walker import Walker

# classes for making the resampler
from alt_maps_distance import UnbindingDistanceAltMaps
#from wepy.resampling.distances.receptor import UnbindingDistance


from wepy.resampling.resamplers.resampler import Resampler
from wepy.resampling.resamplers.revo import REVOResampler
#from wepy.openmm import UNITS
# A standard Boundary condition object for unbinding
from wepy.boundary_conditions.receptor import UnbindingBC
from wepy.util.mdtraj import mdtraj_to_json_topology

# standard reporters
from wepy.reporter.hdf5 import WepyHDF5Reporter
from copy import copy
from copy import deepcopy

## PARAMETERS

# OpenMM simulation parameters
# cubic simulation side length
CUBE_LENGTH = 8.2435*unit.nanometer
# angles of the cubic simulation box
CUBE_ANGLE = 90*unit.degree
# distance cutoff for non-bonded interactions
NONBONDED_CUTOFF = 1.0 * unit.nanometer

# Monte Carlo Barostat
# pressure to be maintained
PRESSURE = 1.0*unit.atmosphere
# temperature to be maintained
TEMPERATURE = 300.0*unit.kelvin
# frequency at which volume moves are attempted
VOLUME_MOVE_FREQ = 50

# Platform used for OpenMM which uses different hardware computation
# kernels. Options are: Reference, CPU, OpenCL, CUDA.

# CUDA is the best for NVIDIA GPUs
PLATFORM = 'CUDA'

# Langevin Integrator
#FRICTION_COEFFICIENT = .001/unit.picosecond
# step size of time integrations
STEP_SIZE = 0.002*unit.picoseconds

# Distance metric parameters, these are not used in OpenMM and so
# don't need the units associated with them explicitly, so be careful!

# distance from the ligand in the crystal structure used to determine
# the binding site, used to align ligands in the Unbinding distance
# metric
BINDING_SITE_CUTOFF = 0.8 # in nanometers

# the residue id for the ligand so that it's indices can be determined

# Resampler parameters

# the maximum weight allowed for a walker
PMAXpmax = 0.1
# the minimum weight allowed for a walker
PMIN = 1e-12

# the maximum number of regions allowed under each parent region
MAX_N_REGIONS = (10, 10, 10, 10)

# the maximum size of regions, new regions will be created if a walker
# is beyond this distance from each voronoi image unless there is an
# already maximal number of regions
MAX_REGION_SIZES = (1, 0.5, .35, .25) # nanometers

# boundary condition parameters

# maximum distance between between any atom of the ligand and any
# other atom of the protein, if the shortest such atom-atom distance
# is larger than this the ligand will be considered unbound and
# restarted in the initial state
CUTOFF_DISTANCE = 0.6 # nm # was 1, changed to 0.6 as of 6/11/19
# reporting parameters

# these are the properties of the states (i.e. from OpenMM) which will
# be saved into the HDF5
SAVE_FIELDS = ('positions', 'box_vectors', 'velocities')
# these are the names of the units which will be stored with each
# field in the HDF5
UNITS = UNIT_NAMES
# this is the frequency to save the full system as an alternate
# representation, the main "positions" field will only have the atoms
# for the protein and ligand which will be determined at run time
ALL_ATOMS_SAVE_FREQ = 10
# we can specify some fields to be only saved at a given frequency in
# the simulation, so here each tuple has the field to be saved
# infrequently (sparsely) and the cycle frequency at which it will be
# saved
SPARSE_FIELDS = (('velocities', 10),
                )


def main(n_runs, n_cycles, steps, n_walkers, fric_coef, outputs_dir, n_workers=1, seed=None):

    ## Load objects needed for various purpose
    print("Main is running")
    FRICTION_COEFFICIENT = fric_coef/unit.picosecond
    print(FRICTION_COEFFICIENT)
    ## INPUTS/OUT
    # the inputs directory
    inputs_dir = osp.realpath('/mnt/home/hallrob3/tspo_files/inputs')
    # the outputs path
   #outputs_dir = osp.realpath('/mnt/home/hallrob3/tspo_files/outputs')
    # make the outputs dir if it doesn't exist
    print(outputs_dir)
    os.makedirs(outputs_dir, exist_ok=True)


    # outputs
    hdf5_filename = 'OAG60_results_REVO.wepy.h5'
    dashboard_filename = 'OAG60_wepy.dash.txt'

    # normalize the output paths
    hdf5_path = osp.join(outputs_dir, hdf5_filename)
    dashboard_path = osp.join(outputs_dir, dashboard_filename)


    inputs_dir = "/mnt/home/hallrob3/tspo_files/inputs"
    pdbfile = osp.join(inputs_dir, 'complex.pdb')
    grofile = osp.join(inputs_dir, "complex.gro")
    topfile = osp.join(inputs_dir, "complex.top")
    sysxml_file = osp.join(inputs_dir, "complex.xml")
    outfile = 'OAG60.wepy.h5'

    with open(sysxml_file, 'r') as rf:
        sys_xml = rf.read()
    system = omm.XmlSerializer.deserialize(sys_xml)

    pdb_traj = mdj.load_pdb(pdbfile)
    omm_gro = omma.GromacsGroFile(grofile)
    omm_top = omma.GromacsTopFile(topfile, periodicBoxVectors=omm_gro.getPeriodicBoxVectors())

    # system = omm_top.createSystem(nonbondedMethod=omma.PME, nonbondedCutoff=1*unit.nanometer,
    #                           constraints=omma.HBonds)


    # TODO: what are these indexing?? The state, image, protein?
    binding_selection_idxs = [100,104,127,122,94,98,92,87]
    alt_maps = []
    tmp = copy(binding_selection_idxs)
    for i in range(int(len(tmp)/2)-1):
        a = tmp.pop()
        tmp = [a] + tmp
        a = tmp.pop()
        tmp = [a] + tmp
        alt_maps.append(copy(tmp))

    print("Binding site: {}".format(binding_selection_idxs))

    json_top = mdtraj_to_json_topology(pdb_traj.topology)
    protein_idxs = pdb_traj.topology.select('resname "HST"')
    lig_idxs = pdb_traj.topology.select('resname "GST"')
    print("Ligand: {}".format(','.join([str(idx) for idx in lig_idxs])))

    # set the box size lengths and angles
    #lengths = [CUBE_LENGTH for i in range(3)]
    #angles = [CUBE_ANGLE for i in range(3)]
    #psf.setBox(*lengths, *angles)

    # charmm forcefields parameters
    #params = omma.CharmmPrameterSet(*charmm_param_paths)

    # create a system using the topology method giving it a topology and
    # the method for calculation
    # make this a constant temperature and pressure simulation at 1.0
    # atm, 300 K, with volume move attempts every 50 steps
    barostat = omm.MonteCarloBarostat(PRESSURE, TEMPERATURE, VOLUME_MOVE_FREQ)

    # add it as a "Force" to the system
    system.addForce(barostat)

    # make an integrator object that is constant temperature
    integrator = omm.LangevinIntegrator(TEMPERATURE,
                                        FRICTION_COEFFICIENT,
                                        STEP_SIZE)

    

    # set up the OpenMMRunner with the system
    runner = OpenMMRunner(system, omm_top.topology, integrator, platform=PLATFORM)

    tmp_sim = omma.Simulation(omm_top, system, integrator)
    tmp_sim.context.setPositions(pdb_traj.openmm_positions(frame=0))
    omm_state = tmp_sim.context.getState(getPositions=True,
                                getVelocities=True,
                                getParameters=True,
                                getForces=True,
                                getEnergy=True,
                                getParameterDerivatives=True)


    # the initial state, which is used as reference for many things
    init_state = OpenMMState(omm_state)
    print(np.shape(init_state))
    ## Make the distance Metric

    # make the distance metric with the ligand and binding site
    # indices for selecting atoms for the image and for doing the
    # alignments to only the binding site. All images will be aligned
    # to the reference initial state
    unb_distance = UnbindingDistanceAltMaps(ligand_idxs=lig_idxs,
                                            binding_site_idxs=binding_selection_idxs,
                                            ref_state=init_state,
                                            alt_maps=np.array(alt_maps))
    ## Make the resampler

    # make a REVO resampler with default parameters and our
    # distance metric

    resampler = REVOResampler(distance=unb_distance,
                              init_state=init_state,
                              # algorithm parameters
                              d0=0.1,
                              dpower=4)
                              #pmin = 1e-12
                              #pmax = 0.1
                              
    
    ## Make the Boundary Condition

    ubc =  UnbindingBC(cutoff_distance=CUTOFF_DISTANCE,
                       initial_state=init_state,
                      topology=json_top,
                      ligand_idxs=lig_idxs,
                      receptor_idxs=protein_idxs)

    ## make the reporters

    # WepyHDF5
    # make a dictionary of units for adding to the HDF5
    # open it in truncate mode first, then switch after first run
    hdf5_reporter = WepyHDF5Reporter(file_path=hdf5_path, mode='w',
                                     # the fields of the State that will be saved in the HDF5 file
                                     save_fields=SAVE_FIELDS,
                                     # the topology in a JSON format
                                     topology=json_top,
                                     # the resampler and boundary
                                     # conditions for getting data
                                     # types and shapes for saving
                                     resampler=resampler,
                                     boundary_conditions=ubc,
                                     # the units to save the fields in
                                     units=dict(UNITS),
                                     # sparse (in time) fields
                                     sparse_fields=dict(SPARSE_FIELDS)
                                     # sparse atoms fields
                                     #main_rep_idxs=np.concatenate((lig_idxs, protein_idxs)),
                                     #all_atoms_rep_freq=ALL_ATOMS_SAVE_FREQ
    )

    reporters = [hdf5_reporter]

    # The work mapper

    # we use a mapper that uses GPUs
    work_mapper = WorkerMapper(worker_type=OpenMMGPUWorker,
                               num_workers=n_workers)

    ## Combine all these parts and setup the simulation manager

    # set up parameters for running the simulation
    # initial weights
    init_weight = 1.0 / n_walkers

    # a list of the initial walkers
    init_walkers = [Walker(OpenMMState(omm_state), init_weight) for i in range(n_walkers)]

    # Instantiate a simulation manager
    sim_manager = Manager(init_walkers,
                          runner=runner,
                          resampler=resampler,
                          boundary_conditions=ubc,
                          work_mapper=work_mapper,
                          reporters=reporters)


    ### RUN the simulation
    for run_idx in range(n_runs):
        print("Starting run: {}".format(run_idx))
        sim_manager.run_simulation(n_cycles, steps)
        print("Finished run: {}".format(run_idx))


if __name__ == "__main__":
    import time
    import multiprocessing as mp
    import sys
    import logging

    # needs to call spawn for starting processes due to CUDA not
    # tolerating fork
    mp.set_start_method('spawn')
    #mp.log_to_stderr(logging.DEBUG)

    if sys.argv[1] == "--help" or sys.argv[1] == '-h':
        print("arguments: n_runs, n_cycles, n_steps, n_walkers, n_workers")
    else:

        n_runs = int(sys.argv[1])
        n_cycles = int(sys.argv[2])
        n_steps = int(sys.argv[3])
        n_walkers = int(sys.argv[4])
        n_workers = int(sys.argv[5])
        fric_coef = float(sys.argv[6])
        output_dir = str(sys.argv[7])
        print("Number of steps: {}".format(n_steps))
        print("Number of cycles: {}".format(n_cycles))

        print("Friction Coefficient : {}".format(fric_coef))
        
        steps = [n_steps for i in range(n_cycles)]

        start = time.time()
        main(n_runs, n_cycles, steps, n_walkers, fric_coef, output_dir, n_workers)
        end = time.time()

        print("time {}".format(end-start))
