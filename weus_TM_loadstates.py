import sys
from copy import copy
import os
import os.path as osp
import pickle
import random

import numpy as np
import simtk.openmm.app as omma

from simtk.openmm.app import *
from simtk.openmm import *
from simtk.unit import *

import simtk.openmm as omm
import simtk.unit as unit

import mdtraj as mdj
from wepy.util.mdtraj import mdtraj_to_json_topology

from wepy.sim_manager import Manager

from distances.one_prop_dist import OnePropertyDistance
from forces.harmonic import HarmonicWEUSCOMForce

from resampling.resamplers.window_exchange import WindowExchangeResampler
from wepy.walker import Walker
from wepy.runners.openmm import OpenMMRunner, OpenMMState, OpenMMGPUWorker, OpenMMGPUWalkerTaskProcess
from runners.dev_weus_runner import OpenMMWindowExchangeRunner
from wepy.runners.openmm import UNIT_NAMES, GET_STATE_KWARG_DEFAULTS
from wepy.work_mapper.task_mapper import TaskMapper
#from wepy.work_mapper.mapper import Mapper
#from wepy.work_mapper.mapper import WorkerMapper

from wepy.reporter.hdf5 import WepyHDF5Reporter
import logging

from copy import deepcopy

# set logging threshold
logging.basicConfig(level=logging.INFO,format='%(asctime)s %(message)s')

# Platform used for OpenMM which uses different hardware computation
# kernels. Options are: Reference, CPU, OpenCL, CUDA.
PLATFORM = 'CUDA'

# Monte Carlo Barostat
PRESSURE = 1.0*unit.atmosphere
TEMPERATURE = 300.0*unit.kelvin
FRICTION_COEFFICIENT = 1/unit.picosecond
STEP_SIZE = 0.002*unit.picoseconds

# the maximum weight allowed for a walker
PMAX = 0.5
# the minimum weight allowed for a walker
PMIN = 1e-100

# reporting parameters

# these are the properties of the states (i.e. from OpenMM) which will
# be saved into the HDF5
SAVE_FIELDS = ('positions', 'box_vectors', 'velocities', 'activity', 'd0_val')
# these are the names of the units which will be stored with each
# field in the HDF5
UNITS = UNIT_NAMES
ALL_ATOMS_SAVE_FREQ = 5
#SPARSE_FIELDS = (('velocities', 5),
#                )

## INPUTS/OUTPUTS

#Read the inputs
if sys.argv[1] == "-h" or sys.argv[1] == "--help":
    print("arguments: n_cycles, n_steps, hdf5_filename")
    exit(0)
else:
    n_cycles = int(sys.argv[1])
    n_steps = int(sys.argv[2])
#    frame_number = int(sys.argv[3])
#    n_walk = int(sys.argv[4])
    hdf5_file = sys.argv[3]

sampl_sys = 'OA-G6-0'
hdf5_filename = hdf5_file

# the inputs directory
inputs_dir = osp.realpath(f'./inputs/SAMPLing/{sampl_sys}/')

pdbfile = osp.join(inputs_dir, 'complex.pdb')
grofile = osp.join(inputs_dir, "complex.gro")
topfile = osp.join(inputs_dir, "complex.top")
sysxml_file = osp.join(inputs_dir, "complex.xml")
dummy_top = osp.join(inputs_dir, "../dummy_top.pdb")
structures = osp.join(inputs_dir, "../US_75poses.dcd")

# the outputs path
outputs_dir = osp.realpath(f'/dickson/s2/roussey1/we_us/outputs/taskmapLS_75/')

#outputs_dir = osp.realpath('outputs/test/')

# make the outputs dir if it doesn't exist
os.makedirs(outputs_dir, exist_ok=True)

pdb_traj = mdj.load_pdb(pdbfile)
dummy_traj = mdj.load_pdb(dummy_top)
dcd  = mdj.load_dcd(structures, pdb_traj.topology)
dummy_traj = mdj.load_pdb(dummy_top)
tmp_top = dummy_traj.topology # 2 atom system for COM to COM calc

n_walkers = dcd.n_frames
omm_gro = omma.GromacsGroFile(grofile)
omm_top = omma.GromacsTopFile(topfile, periodicBoxVectors=omm_gro.getPeriodicBoxVectors())
d0_numbers = [0.315 + 15*i*0.0015 for i in range(75)]

mdj_top = pdb_traj.topology.select('all')
tmp_top = dummy_traj.topology
json_top = mdtraj_to_json_topology(pdb_traj.topology)

omm_states = []
get_state_kwargs = dict(GET_STATE_KWARG_DEFAULTS)
systems = []
#n_walkers = 4

with open('systems75.pkl','rb') as f:
        systems = pickle.load(f)
with open('omm_states75.pkl','rb') as f:
        omm_states = pickle.load(f)

force = systems[0].getForce(5)
bond_params = force.getBondParameters(0)
k_init = bond_params[1][0]

hst_idxs = pdb_traj.topology.select('resname "HST"')
gst_idxs = pdb_traj.topology.select('resname "GST"')
integrator = omm.LangevinIntegrator(TEMPERATURE, FRICTION_COEFFICIENT, STEP_SIZE)

runner = OpenMMWindowExchangeRunner(systems[0], omm_top.topology, integrator, platform='CUDA',
                                          harmonic_force=HarmonicWEUSCOMForce(k_init=k_init, d0_init=d0_numbers[0]))

## Resampler
Kb = 0.00831446 #kj/(mol*K)
Beta = 1/(TEMPERATURE*Kb)
n_resamp_steps = n_walkers

neighbors = list(map(lambda x: [x+1] if x==0 else ([x-1] if x==n_walkers-1 else [x-1, x+1]), [i for i in range(n_walkers)]))
resampler = WindowExchangeResampler(no_swap_keys=['d0_val'], neighbor_list=neighbors, gen_neighbor_list=False, topology=tmp_top, hst_idxs=hst_idxs, gst_idxs=gst_idxs, beta=Beta, k=k_init, n_steps=n_resamp_steps)

## Reporters
# make a dictionary of units for adding to the HDF5
units = dict(UNIT_NAMES)

mapper = TaskMapper(walker_task_type=OpenMMGPUWalkerTaskProcess,
                    num_workers=8,
                    platform='CUDA',
                    device_ids=[0,1,2,3,4,5,6,7])
#                    proc_start_method='spawn')

## Run the simulation

if __name__ == "__main__":

        
        # normalize the output paths
        hdf5_path = osp.join(outputs_dir, hdf5_filename)

        print("Number of steps: {}".format(n_steps))
        print("Number of cycles: {}".format(n_cycles))

        # create the initial walkers
        init_weight = 1.0 / n_walkers
        d0_numbers = [0.315 + 15*i*0.0015 for i in range(75)]

        # unless init positions are different, init omm_state is the same for all walkers
        init_walkers = [Walker(OpenMMState(omm_states[i], d0_val=np.array([d0_numbers[i]])), init_weight) for i in range(n_walkers)]

        hdf5_reporter = WepyHDF5Reporter(file_path=hdf5_path, mode='w',
                                         # save_fields set to None saves everything
                                         save_fields=None,
                                         resampler=resampler,
                                         boundary_conditions=None,
                                         topology=json_top,
                                         units=units)
        reporters = [hdf5_reporter]
        #import ipdb; ipdb.set_trace()
        
        sim_manager = Manager(init_walkers,
                              runner=runner,
                              resampler=resampler,
                              boundary_conditions=None,
                              work_mapper=mapper,
                              reporters=reporters)
        # make a number of steps for each cycle. In principle it could be
        # different each cycle
        steps = [n_steps for i in range(n_cycles)]

       # actually run the simulation
        print("Starting run: {}".format(0))
        sim_manager.run_simulation(n_cycles, steps)
        print("Finished run: {}".format(0))


        print("Finished first file")
