import numpy as np
import mdtraj as mdj
from numpy import linalg as LA
#import matplotlib.pyplot as plt
import sys

dcd_path = sys.argv[1]

pdb = mdj.load_pdb('complex.pdb')
dcd = mdj.load_dcd(dcd_path, top=pdb.topology)
topology = pdb.topology

frames = dcd.n_frames
print(frames)
hst_idxs = topology.select('resname HST')
gst_idxs = topology.select('resname GST')

dists = []

for i in range(frames):

    pos = dcd.xyz[i]
    hst_pos = pos[hst_idxs]
    gst_pos = pos[gst_idxs]

    hst_com = np.mean(hst_pos, axis=0)
    gst_com = np.mean(gst_pos, axis=0)

    dist = LA.norm(hst_com - gst_com)
    dists.append(dist)


    
print(dists)
