from collections import namedtuple, defaultdict
from enum import Enum
import logging

import numpy as np

from wepy.resampling.decisions.decision import Decision

#from wepy.walker import split, keep_merge

# the possible types of decisions that can be made enumerated for
# storage, these each correspond to specific instruction type
class SwapDecisionEnum(Enum):
    """Enum definition for cloning and merging decision values."

    - NOTHING : 1
    - SWAP : 2
    
    """

    NOTHING = 1
    """Do nothing with the walker sample. """
    
    SWAP = 2
    """Use another walkers state for the next cycle."""


class SwapDecision(Decision):
    """Decision encoding swapping decisions for replica exchange or
    Window Exchange Umbrella Sampling simulations.

    The decision records have in addition to the 'decision_id' a field
    called 'target_idxs'. This field has differing interpretations
    depending on the 'decision_id'.

    For NOTHING it indicates the walker index to assign
    this sample to after resampling. In this sense the walker is
    merely a vessel for the propagation of the state and acts as a
    slot.

    For SWAP it indicates the walker whose state will be used to 
    continue this walker's trajectory for the next cycle.  Note that in
    Window Exchange Umbrella Sampling simulations, the external forces
    are constant for a given walker, and only the positions, velocities,
    and box vectors are copied from walker to walker.

    """

    FIELDS = Decision.FIELDS + ('target_idxs',)
    SHAPES = Decision.SHAPES + (Ellipsis,)
    DTYPES = Decision.DTYPES + (np.int,)
        
    RECORD_FIELDS = Decision.RECORD_FIELDS + ('target_idxs',)

    ENUM = SwapDecisionEnum
    DEFAULT_DECISION = ENUM.NOTHING
    ANCESTOR_DECISION_IDS = (ENUM.NOTHING.value,
                             ENUM.SWAP.value)

    # TODO deprecate in favor of Decision implementation
    @classmethod
    def record(cls, enum_value, target_idxs):
        record = super().record(enum_value)
        record['target_idxs'] = target_idxs

        return record

    @classmethod
    def action(cls, walkers, decisions, no_swap_keys):

        # list for the modified walkers
        mod_walkers = [None for i in range(len(walkers))]
        
        # perform swaps for each step of resampling
        for step_idx, step_recs in enumerate(decisions):
                                 
            # we need to collect groups of merges, one entry for each
            # merge, where the key is the walker_idx of the keep merge slot
#            squash_walkers = defaultdict(list)
#            keep_walkers = {}
            # go through each decision and perform the decision
            # instructions
            for walker_idx, walker_rec in enumerate(step_recs):

                decision_value = walker_rec['decision_id']
                instruction = walker_rec['target_idxs']

                if decision_value == cls.ENUM.NOTHING.value:
                    # check to make sure a walker doesn't already exist
                    # where you are going to put it
                    if mod_walkers[instruction[0]] is not None:
                        raise ValueError(
                            "Multiple walkers assigned to position {}".format(instruction[0]))

                    # put the walker in the position specified by the
                    # instruction
                    mod_walkers[walker_idx] = walkers[walker_idx]

                # for a swap
                elif decision_value == cls.ENUM.SWAP.value:

                    # get the swapped walker
                    mod_walkers[walker_idx] = walkers[instruction[0]]

                else:
                    raise ValueError("Decision not recognized")

        # keep the no_swap attributes constant
#        import ipdb; ipdb.set_trace()
        for attr in no_swap_keys:
            # get d0 vals and store in list to properly set d0 for each walker
            d0s = [w.state[attr] for w in walkers]
            for i in range(len(walkers)):
                mod_walkers[i].state._data[attr] = d0s[i]#walkers[i].state[attr]

        if not all([False if walker is None else True for walker in mod_walkers]):

            raise ValueError("Some walkers were not created")

        return mod_walkers
