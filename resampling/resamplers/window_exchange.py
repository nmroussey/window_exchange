import multiprocessing as mulproc
import random as rand
import itertools as it

import numpy as np
import mdtraj as mdj

from wepy.resampling.resamplers.resampler import Resampler
from resampling.decisions.swap import SwapDecision
from wepy.util.util import box_vectors_to_lengths_angles

class WindowExchangeResampler(Resampler):
    """

    """
    # state change data for the resampler
    RESAMPLER_FIELDS = ()
    RESAMPLER_SHAPES = ()
    RESAMPLER_DTYPES = ()

    DECISION = SwapDecision

    # fields for resampling data
    RESAMPLING_FIELDS = DECISION.FIELDS + ('step_idx', 'walker_idx',)
    RESAMPLING_SHAPES = DECISION.SHAPES + ((1,), (1,),)
    RESAMPLING_DTYPES = DECISION.DTYPES + (np.int, np.int,)

    # fields that can be used for a table like representation
    RESAMPLING_RECORD_FIELDS = DECISION.RECORD_FIELDS + ('step_idx', 'walker_idx',)

    def __init__(self, seed=None, no_swap_keys=[], neighbor_list=[], gen_neighbor_list=None, topology=None, hst_idxs=None, gst_idxs=None, beta=None, k=None, n_steps=None):

        self.no_swap_keys = no_swap_keys
        self.neighbor_list = neighbor_list
        self.topology= topology
        self.hst_idxs = hst_idxs
        self.gst_idxs = gst_idxs
        self.beta = beta  #mol/kJ
        self.k = k # kJ/mol/nm^2
        self.n_steps = n_steps
        
        # setting the random seed
        self.seed = seed
        if seed is not None:
            rand.seed(seed)


    def _Calc_COMtoCOM(self, walker=None):

        # get the COM to COM per walkers
        pos = walker.state.positions._value

	# make dummy atoms representing the COM of the host and guest                               
        hst_pos = pos[self.hst_idxs]
        gst_pos = pos[self.gst_idxs]
        hst_com = np.mean(hst_pos, axis=0)
        gst_com = np.mean(gst_pos, axis=0)

        small_pos = np.array([hst_com, gst_com])

        # get the distance for the current state                                                     
        box_vectors = walker.state.box_vectors._value
        unitcell_lengths, unitcell_angles = box_vectors_to_lengths_angles(box_vectors)
        unitcell_lengths = np.array(unitcell_lengths)

        # self.topology here is a 2 atom pdb used to represent
        # the hst_com and gst_com 
        traj = mdj.Trajectory(small_pos, self.topology,
                              unitcell_lengths=unitcell_lengths,
                              unitcell_angles=unitcell_angles)

        dist = mdj.compute_distances(traj, [(0,1)], periodic=True)
        
        return dist
    
    def _gen_1D_neighbor_list(self, n_walkers):
        
        # have this set to not be used, not sure how to implement
        # have set to pass the same list in in the main weus_resamp.py script

        return list(map(lambda x: [x+1] if x==0 else ([x-1] if x==n_walkers-1 else [x-1, x+1]),
                        [i for i in range(n_walkers)]))

    def _get_energy(self, walker_op_value, system_constant):
        # in kJ/mol
        harmonic_energy = 0.5 * self.k * (walker_op_value - system_constant)**2
        
        return harmonic_energy

    def decide_clone_merge(self, walker_op_values, system_constants):

        walker_idxs = [i for i in range(len(walker_op_values))]
        walker_actions = [None for i in range(len(walker_op_values))]

        for step in range(self.n_steps):

            # pick a random walker (wi)
            wi = rand.choice(walker_idxs) # walker_idx
            
            # pick a random neighbor from neighbor list (ni)
            ni = rand.choice(self.neighbor_list[wi]) # neighbor_idx

            # get current sum of energies
            e0 = self._get_energy(walker_op_values[walker_idxs[wi]],system_constants[wi]) + self._get_energy(walker_op_values[walker_idxs[ni]],system_constants[ni])

            # calculate sum of energies if walkers were swapped
            e_swap = self._get_energy(walker_op_values[walker_idxs[wi]],system_constants[ni]) + self._get_energy(walker_op_values[walker_idxs[ni]],system_constants[wi])

            if e_swap < e0:

                # great! accept swap
                tmp = walker_idxs[wi]
                walker_idxs[wi] = walker_idxs[ni]
                walker_idxs[ni] = tmp

            else:
                # test random number
                a = np.random.random()
                if a < np.exp(-self.beta*(e_swap-e0)):
                    # do the swap
                    tmp = walker_idxs[wi]
                    walker_idxs[wi] = walker_idxs[ni]
                    walker_idxs[ni] = tmp

        for walker_idx, target_idx in enumerate(walker_idxs):
            # create record 
                            
            if walker_idx == target_idx:
                walker_actions[walker_idx] = self.decision.record(self.decision.ENUM.NOTHING.value,
                                                             target_idxs=tuple([target_idx]))
            else:
                walker_actions[walker_idx] = self.decision.record(self.decision.ENUM.SWAP.value,
                                                                  target_idxs=tuple([target_idx]))

        for walker_idx, walker_record in enumerate(walker_actions):
            walker_record['step_idx'] = np.array([0])
            walker_record['walker_idx'] = np.array([walker_idx])
#        import ipdb; ipdb.set_trace()
        return walker_actions

    def resample(self, walkers, debug_prints=False):

        n_walkers = len(walkers)

        # calculate importance values
        system_constants = np.array([w.state['d0_val'] for w in walkers])
        walker_op_values = np.array([self._Calc_COMtoCOM(w) for w in walkers])

        if debug_prints:
            print("System constants:")
            print(system_constants)
            print("Walker dists:")
            print(walker_op_values)
            
        # determine cloning and merging actions to be performed
        resampling_data = self.decide_clone_merge(walker_op_values, system_constants)

        # convert the target idxs and decision_id to feature vector arrays
        for record in resampling_data:
            record['target_idxs'] = np.array(record['target_idxs'])
            record['decision_id'] = np.array([record['decision_id']])

        # actually do the cloning and merging of the walkers
        resampled_walkers = self.decision.action(walkers, [resampling_data], self.no_swap_keys)

        # flatten the distance matrix and give the number of walkers
        # as well for the resampler data, there is just one per cycle

        ## is there anything to add here?
        #resampler_data = [{'n_walkers' : np.array([len(walkers)])}]
        resampler_data = []
#        import ipdb; ipdb.set_trace()
        return resampled_walkers, resampling_data, resampler_data

